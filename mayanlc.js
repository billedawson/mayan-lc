// Node.js module for Mayan long count


//  Does module exist?
exports.hello = function ()
{
  return "hello, Mayan world!";
};


//  Return Mayan Long Count for today
exports.today = function ()
{
  // Init variable to store remainder
  var longStart = 584282.5; // GMT correlation
  var today = new Date();
  var ourYear = today.getFullYear();
  var ourSeconds = today.getTime();

  ourSeconds = ourSeconds/1000.0;
  var mayaDay = Math.round(ourSeconds);
  mayaDay /= 86400;
  mayaDay += 2440588;
  mayaDay = Math.floor(mayaDay);
  mayaDay -= longStart;

  //var dateDiff = today - longStart;
  var mDays = mayaDay;

  // Baktun (144000)
  var baktun = Math.floor(mayaDay/144000);
  mDays = mDays - (baktun*144000);

  // Katun (7200)
  var katun = Math.floor(mDays/7200);
  mDays = mDays - (katun*7200);

  // Tun (360)
  var tun = Math.floor(mDays/360);
  mDays = mDays - (tun*360);

  // Uinal (20)
  var uinal = Math.floor(mDays/20);
  mDays = mDays - (uinal*20);

  // Kin
  var kin = Math.floor(mDays);

  // Return Long Count
  // "Baktun.Katun.Tun.Uinal.Kin"
  var longCount = baktun + "." + katun + "." + tun + "." + uinal + "." + kin;
  return longCount;
}

// Return Gregorian date for MLC
exports.mlcDate = function (uToday) {
  
  // work in progress
  var myDate = "whaddup, Gregory?";

  return myDate;

}
